package datastructs.linkedlist;

/*
Desc:
This is my implementation of the linked list (using java).
TODO:
    - After completion, add generic (templatized) data type.
Tests:

*/

public class ll {
    // This is node for list
    public class Node {
        int data;
        public Node next;
        // Ctor - set data field
        public void Node(int elVal) { this.data = elVal; }
        // Ctor - empty
        public void Node() { this.data = 0; }
        // Set next node       
        public void setNode(Node n) { next = n; }      
        // Set data field 
        public void setVal(int elVal) { data = elVal; }
    }

    private Node head = null;
    private Node tail = null;

    // Add element to end of the list
    public void addEl(int elVal) {
        // case when list is empty
        if (head == tail && head == null) {
            tail = new Node(elVal);
            head = tail;
            tail.next = null;
        }
        // case when list has elements
        else {
            tail.next = new Node();    // next-> points to new Node
            tail.next.setVal(elVal);
            tail.next.next = null;     // next->next-> points to null. End of the list
        }
    }

    // Insert el. to the head
    public void insertHead(int elVal) {
        Node tmp;
        // list is empty
        if (head == null) {
            head = new Node();
            head.setVal(elVal);
        }
        // list is not empty
        else {
            tmp = head.next;
            head.next = new Node();
            head.next.setVal(elVal);
            head.next.next = tmp;
        }
    }

    // Insert el. to the tail
    public void insertTail(int elVal) {

    }

    // Insert el. at the specified position
    public void insertEl(int elVal, int pos) {
        int i = 0;
        Node prev, tmp;
        // iterate over the list
        for (Node it = head; it != tail && i <= pos; it=it.next) {
            prev = it;
            i++;
        }
        // insert element if the position
        if (i==pos) {
            tmp = prev.next.next;
            prev.next.next = new Node(elVal);
            prev.next.next.next = tmp;
        }
    }

    public void deleteEl(int elVal) {
    }

    // Return index for the element, if found
    // else, return -1 if element is not found
    public Node findElement(int elVal) {
    }

    // testing function
    public static void main(String[] args) {
        //try {
        //System.out.println(myNode.getData());
        //}
        //catch (Exception e) {
        //    e.PrintStackTrace();
        //}
    }
}
