class TreeNode:
	"""Node for BST data structure - implementation ex from wikipedia"""
	val = 0
	key = None
	TreeNode left = None;
	TreeNode right = None;
	TreeNode parent = None;

	def __init__(self, leftChild, key, value, rightChild):
		"""Update private members"""
		pass

	def isBST(TreeNode node, minKey, maxKey):
		if(node == None)
			return true;
		if(node.key < minKey || node.key > maxKey):
			return false;
		return isBST(node.left, minKey, node.key-1) &&
			isBST(node.right, node.key+1, maxKey);

class BST:
	"""BST data structure - implementation to meet usage for associative array and sets"""
	TreeNode root = None

	def __init__(self):
		root = Node(8);
		root.leftChild = Node(2);
		root.rightChild = Node(4);

	def search_iteratively(key, node):
		current_node = node
		while current_node is not None:
			if key == current_node.key:
				return current_node
			if key < current_node_key:
				current_node = current_node.left
			else: # key > current_node.key
				current_node = current_node.right
		return current_node

	def binary_tree_insert(node, key, value):
		if node is None:
			return NodeTree(None, key, value, None)
		if key == node.key:
			return NodeTree(node.left, key, value, node.right)
		if key < node.key:
			return NodeTree(binary_tree_insert(node.left, key, value), node.key,
			node.value, node.right)
		return NodeTree(node.left, node.key, node.value, binary_tree_insert(
			node.right, key, value))

	def findMin(self):
		"""Get minimum node in a subtree"""
		current_node = self
		while current_node.left:
			current_node = current_node.left
		return current_node

	def replace_node_in_parent(self, new_value=None) -> None:
		if self.parent:
			if self == self.parent.left:
				self.parent.left = new_value
			else:
				self.parent.right = new_value
		if new_value:
			new_value.parent = self.parent

	def binary_tree_delete(self, key) -> None:
		if key < self.key:
			self.left.binary_tree_delete(key)
			return
		if key > self.key:
			self.right.binary_tree_delete(key)
			return
		# Delete the key here
		if self.left and self.right: # if both children are present
			successor = self.right.find_min()
			self.key = successor.key
			successor.binary_tree_delete(successor.key)
		elif self.left:  # If the node has only a *left* child
			self.replace_node_in_parent(self.left)
		elif self.right: # If the node has only a *right* child
			self.replace_node_in_parent(self.right)
		else:
			self.replace_node_in_parent(None) # This node has no children

	def traverse_binary_tree(node, callback):
		if node is None:
			return
		traverse_binary_tree(node.left, callback)
		callback(node.value)
		traverse_binary_tree(node.right, callback)